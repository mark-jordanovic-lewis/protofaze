# Protofaze
### Phase One
#### The Simplest PII - tsPII
Attempt at lightweight, token based PII user microservice using hashicorp vault and postgres.

- Postgres 
    -User table
    |   column   |     type     |                           use                           |
    |------------|--------------|---------------------------------------------------------|
    | id         | serial       | std postgres id                                         |
    | name       | varchar(20)  | (some string)                                           |
    | uniquename | varchar(60)  | identifier for pwd validation email commonly            |
    | uuid       | uuid         | link external entities and not leak any data            |
    | pwd        | varchar(128) | hashed and encrypted pwd                                |
    | ips        | inet[]       | List of IP addrs used to connect (not necc. to use)     |
    | _data      | json         | (todo encrypt PII data -> text)                         |
    | token      | varchar(128) | authentication/session token (use it for what you want) |
    -  todo: Data Input Table (?)
        - rows are removed immediately after encryption/decryption
    | uuid | data |
    
- Vault
    - fronts postgres _data column.
    
- Authentication and Validation api (golang)
    - provides v simple api to CRUD of users
    - secure access to user _data column (update and read only)

### Todo
- Delete users
- User table grouping
- Renew password
- User data delete
- Golang login API
- Containerise postgres/vault/go for microservice deployment
- Token validation via comparisson with uuid
- GoLang Listen daemon for update to DataInputTable followed by encryption call to vault
    - keep json keys but encrypt content
    
### Better names
- PII-HEAD
    - Holds Encrypted Arbitrary Data
- tsPII
    - the simplest PII

## Utilities to know
#### openssl
Use this to generate your signed ca-certs and keys for both postgres and vault. In the vault case you 
will need to edit `/etc/ssl/openssl.cnf` to include the addresses you are opening up the vault listeners 
on (in config.hcl in the data folders) before you generate any certs.

- generate the private key and .req file (change psql to vault for vault)
```bash
openssl req -x509 \
    -batch \
    -nodes \
    -newkey rsa:2048 \
    -keyout ss_psql_cert.key \      # ca_cert key
    -out ss_psql_cert.crt \         # ca_cert (selfsigned)
    -config ss_postgres_cert.cfr \  # openssl cert generating config
    -days 365                       # certificate ttl
```

- build the key from the private key
```bash
openssl rsa -in privkey.pem -out <certname>.key
```

- build the cert from the key and req
```bash
openssl req -x509 -in <certname>.req -text -key <certname>.key -out <certname>.crt
```

You may want to rename the private and move it and the .req into a file that you can forget 
about and go searching for later.
openssl req -x509 -batch -nodes -newkey rsa:2048 -keyout ss_psql_cert.key -out ss_psql_cert.crt -config ss_postgres_cert.cfr -days 365 
