create or replace function reset_user_table() returns boolean as $func$
    begin
        create extension if not exists "pgcrypto";
        drop table if exists users;
        create table if not exists users (
            id serial unique primary key,
            name varchar(20),
            unique_name varchar(60) unique not null,
            uuid uuid unique not null,
            pwd varchar(128) not null,
            vault_token_history varchar(27)[],
            _data json,
            access_token varchar(27) unique,
            vault_token varchar(27) unique );
        create index on users (unique_name);
        create index on users (access_token);
        create index on users (vault_token);
        create or replace function users_uuid() returns trigger as $f$
            declare
                uuid uuid := (select gen_random_uuid());
            begin
                NEW.uuid = uuid;
                return NEW;
            end
        $f$ language plpgsql;
        create trigger assign_uuid before insert on users for each row execute procedure users_uuid();
        create or replace function encrypt_user_pwd() returns trigger as $f$
            declare
                tmp     varchar(128) := (select digest(NEW.pwd, 'sha256'));
                new_pwd varchar(128) := (select crypt(tmp, gen_salt('bf', 8)));
            begin
                if length(NEW.pwd) < 12 then
                    raise exception 'insufficient password length' using hint = 'Password needs to be 12 characters or more.';
                end if;
                NEW.pwd = new_pwd;
                return NEW;
            end
        $f$ language plpgsql;
        create trigger encrypt_password before insert or update on users for each row execute procedure encrypt_user_pwd();
        create or replace function check_password(supplied_email varchar(60), supplied_pwd varchar(128)) returns boolean as $f$
            declare
                pwd varchar(128) := (select users.pwd from users where users.unique_name = supplied_email);
            begin
                supplied_pwd = (select digest(supplied_pwd, 'sha256'));
                return crypt(supplied_pwd, pwd) = pwd;
            end
        $f$ language plpgsql;

        return true;
    end
$func$ language plpgsql;
select reset_user_table();
