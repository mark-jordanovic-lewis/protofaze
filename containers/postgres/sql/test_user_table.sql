-- =========================== --
-- Testing user table function --
-- =========================== --
create type test_return as (password_check_test boolean, password_encrypt_test boolean, uuid_test boolean, rebuilt_table boolean);
create or replace function check_user_table_setup() returns test_return as $f$
declare
    test1 boolean;
    test2 boolean;
    test3 boolean;
    test4 boolean;
begin
    insert into users (name, unique_name, pwd) values ('mark', 'regular@id.slug', 'encrypt_me_please');
    test1 = (select check_password('regular@id.slug', 'encrypt_me_please')::boolean);
    test2 = (select count(pwd) from users where users.pwd in ('encrypt_me_pls', digest('encrypt_me_pls', 'sha256')::text))::integer = 0;
    test3 = (select count(uuid) from users where uuid is null)::integer = 0;
    test4 = (select reset_user_table());
    return (test1, test2, test3, test4);
end
$f$ language plpgsql;
create or replace function test_user_table() returns text as $f$
declare
    test_response varchar(52);
    result test_return = (select check_user_table_setup());
begin
    if result = (true,true,true,true) then
        perform reset_user_table();
        drop function reset_user_table();
        drop function test_user_table();
        alter table users owner to protofaze;
        alter function users_uuid() owner to protofaze;
        alter function encrypt_user_pwd() owner to protofaze;
        alter function check_password(supplied_email varchar(60), supplied_pwd varchar(128)) owner to protofaze;
        test_response = 'PASSED - Generator removed';
    else
        test_response = 'FAILED - Generator remains in the schema\n';
        if !result.password_check_test then
            test_response = test_response || 'check_password failed\n';
        end if;
        if !result.password_encrypt_test then
            test_response = test_response || 'password encryption test failed';
        end if;
        if !result.uuid_test then
            test_response = test_response || 'uuid insertion test failed';
        end if;
        if !result.rebuilt_table then
            test_response = test_response || 'could not rebuild table';
        end if;
    end if;
    return test_response;
end
$f$ language plpgsql;
select test_user_table();
