create user protofaze;
alter user protofaze with encrypted password 'protofaze';
create database protofaze with owner protofaze;
grant all privileges on database protofaze to protofaze;
