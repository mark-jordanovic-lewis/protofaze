#!/usr/bin/env bash

set -e

# Move to sql build dir
cd /sql_build_functions
# Set postgres user's pwd for login
export PGPASSWORD=postgres
# Add the protofaze user and DB
psql -U postgres -a -f add_protofaze_user_and_db.sql
# Build the user table and associated login functions
psql -U postgres -a -d protofaze -f initialise_user_table.sql
# Run user table tests
test_result=$(psql -U postgres -qt -d protofaze -f test_user_table.sql)
if [[ $test_result =~ "PASSED - Generator removed" ]]; then
    echo "Postgres setup for PII-HEAD use"
else
    echo $test_result
    echo "Please inspect the database for errors."
    exit 1
fi
# check that ownership of the users table has been passed on to protofaze
test_result=$(psql -U protofaze -qt -d protofaze -c 'select * from users;')
if [[ "$test_result" != "" ]]; then
   echo "protofaze user not setup correctly - check database"
   exit 1
fi
echo $test_result
# Add the vault secrets table
psql -U protofaze -a -d protofaze -f vault_secret_table.sql
cd -
rm -r sql_build_functions
echo "Postgres protofaze initialise complete"
