#!/bin/bash -e

# wait until the keys are available from the init stage
until [ -f /etc/vault/unseal_data/keys ]; do
  >&2 echo "Waiting for keys - sleeping"
  sleep 2
done
sleep 5

key_1=$(awk 'NR==1' file | cut -d' ' -f4)
key_2=$(awk 'NR==2' file | cut -d' ' -f4)
key_3=$(awk 'NR==3' file | cut -d' ' -f4)
key_4=$(awk 'NR==4' file | cut -d' ' -f4)
key_5=$(awk 'NR==5' file | cut -d' ' -f4)
root_token=$(awk 'NR==6' file | cut -d' ' -f)

# unseal the vault
vault server -certs=/etc/vault/certs.hcl &
vault operator unseal $key_1
vault operator unseal $key_2
vault operator unseal $key_3

# add the policies for the

# read the keys
# put the data into the certs file
# run vault as bg process
# unseal the vault with the keys
# add policies for transit/encryption user
