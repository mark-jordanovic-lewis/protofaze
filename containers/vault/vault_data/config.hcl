ui = true
storage "postgresql" {
  connection_url = "postgres://protofaze:protofaze@localhost:5432/protofaze?sslmode=verify-full"
}
listener "tcp" {
  address     = "127.0.0.1:8200"
  tls_disable = 1
}
telemetry {
  statsite_address = "127.0.0.1:8125"
  disable_hostname = true
}
seal "transit" {
  address            = "https://vault:8200"
  token              = "s.Qf1s5zigZ4OX6akYjQXJC1jY"
  disable_renewal    = "false"

  // Key configuration
  key_name           = "transit_key_name"
  mount_path         = "transit/"
  namespace          = "ns1/"

  // TLS Configuration
  tls_ca_cert        = "/etc/vault/ca_cert.pem"
  tls_client_cert    = "/etc/vault/client_cert.pem"
  tls_client_key     = "/etc/vault/ca_cert.pem"
  tls_server_name    = "vault"
  tls_skip_verify    = "false"
}
