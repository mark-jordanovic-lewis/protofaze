#!/usr/bin/env bash
docker build --tag protofaze:init_vault -f InitVaultDockerfile .
docker run protofaze:init_vault
docker build --tag protofaze:vault -f VaultDockerfile .
