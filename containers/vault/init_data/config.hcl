storage "postgresql" {
  connection_url = "postgres://protofaze:protofaze@172.0.0.3:5432/protofaze"
  table          = "vault_kv_store"
  max_parallel   = "128"
}
listener "tcp" {
  address     = "127.0.0.1:8200"
  tls_disable = 0
  tls_key_file  = "/etc/vault/server-127.key"
  tls_cert_file = "/etc/vault/server-127.crt"
}
listener "tcp" {
  address       = "172.0.0.2:8200"
  tls_disable   = 0
  tls_key_file  = "/etc/vault/server-172.key"
  tls_cert_file = "/etc/vault/server-172.crt"
}
api_addr = "https://0.0.0.0:8200"
backend "inmem" {}
disable_cache = true
disable_mlock = true
# ui = true
# max_lease_ttl = "10h"
# defautl_ttl   = "10h"
# cluster_name  = "pf_vault"
# raw_storage_endpoint = true
# disable_sealwrap = true
# disable_printable_check = true
