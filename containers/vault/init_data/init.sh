#!/bin/sh -e
echo Initialising vault
echo '=================='
echo '- starting  server'
vault server /
        -certs=/etc/vault/certs.hcl \
        -tls-skip-verify \
        & PID=$!
sleep 3
echo '- initialising operator, and acquiring keys'
vault operator init \
  | grep "Unseal Key\|Root Token" \
  > /etc/vault/unseal_data/keys
if [ -f /etc/vault/unseal_data/keys ]; then
  echo '- shims file saved at /etc/vault/unseal_data/keys'
  cat /etc/vault/unseal_data/keys
else
  echo "- could not save keys"
fi
echo '- closing server'
kill -9 $PID
