package api

import (
	"net/http"
	"github.com/julienschmidt/httprouter"

	"database/sql"
	"github.com/lib/pq"
)



type server struct {
	db     *someDatabase
	router *someRouter
	email  EmailSender
}


/* todo: PHASE ONE
	--- DATABASE
	- containerise postgres and connect via DataGrip - DONE
		- read in user table build file     - DONE
		- build and tear down user table    - DONE
		- add user and check password       - DONE
		- tests for user table and triggers - DONE

	--- VAULT
	- add hashicorp-vault to the docker container and run postgres connection through that
		- build up encryption DB
		- automate build of vault through golang/bash/docker
		- add encryption root user
		- tests for entire build process
			- build vault
			- unseal and pull out shards
			- make and access paths

*/
/* todo: phase two

	--- DATABASE
		- Delete User
			- Only delete, no update.
  			  If want new pwd then you must pull all your data,
			  create a new user put the data back in.

	--- First Stage API

*/
/* todo: FUTURE

	--- DATABASE
		- Encrypt docker volume storege:
			https://docs.docker.com/storage/storagedriver/device-mapper-driver/
			Configure direct-lvm mode manually

	--- API Vault users
		- SecurityService (root):
			- vault sealing
			- alert messaging
			- token based metric gathering and flagging (no identifiable links after session ends)
		- VaultService (root):
			- new vault token generation
			- rotating of vault tokens
			- path addition/removal when users create/destroy
		- UserService:
			- encrypt/decrypt of data
			- rotating of access tokens

	--- User Data encrypt/decrypt api
		- add vault encryption api to write to users data (dont encrypt keys, only content of the json)
		- add path for encryption secrets for each user (uuid - do not share)
		- add token generation on login
		- add vault token for users encryption secrets on login
		- tests for creating users, adding data, reading data (valid vs invalid)



--- API SERVER
	- endpoint to add user
	- endpoint to check user password
 */

