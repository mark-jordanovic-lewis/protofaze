create extension if not exists "pgcrypto";
create extension if not exists "uuid-ossp";
--- USER TABLE ---
--- ========== ---
create table if not exists users (
            id serial unique primary key,
            name varchar(20),
            unique_name varchar(60) unique not null,
            uuid uuid,
            password varchar(128) not null,
            ips  inet[],
            _data json,
            token varchar(128) unique);
        create index on users (unique_name);
        create index on users (token);

--- UUID INSERT ---
--- =========== ---
create or replace function users_uuid() returns trigger as $func$
    declare
       uuid uuid := (select gen_random_uuid());
    begin
        NEW.uuid = uuid;
        return NEW;
    end
$func$ language plpgsql;
create trigger assign_uuid before insert on users for each row execute procedure users_uuid();

--- PWD ENCRYPT ---
--- =========== ---
create or replace function encrypt() returns trigger as $func$
    declare
        tmp varchar(128) := (select digest(NEW.password, 'sha256'));
        new_pwd varchar(128) := (select crypt(tmp, gen_salt('bf', 8)));
    begin
        if length(NEW.password) < 12 then
            raise exception 'insufficient password length'
                using hint = 'Password needs to be 12 characters or more.';
        end if;
        -- add a regex check for complexity here.
        NEW.password = new_pwd;
        return NEW;
    end
$func$ language plpgsql;

create trigger encrypt_password
    before insert or update on users
    for each row execute procedure encrypt();

--- LOGIN CHECK ---
--- =========== ---
create or replace function check_password(supplied_email varchar(60), supplied_pwd varchar(128))
    returns boolean as
    $func$
    declare
        pwd varchar(128) := (select users.password from users where users.unique_name = supplied_email);
    begin
        supplied_pwd = (select digest(supplied_pwd, 'sha256'));
        return crypt(supplied_pwd, pwd) = pwd;
    end
    $func$ language plpgsql;


--- TOKEN CHECK ---
--- =========== ---

--- DUMMY DATA ---
--- ========== ---
--- users
-- Without the table trigger
-- do $$
--     declare
--         user_pwd varchar(128) := (select crypt((select digest('secret', 'sha256'))::varchar(128), gen_salt('bf', 8)));
--     begin
--         insert into users (name, email, password) values ('mark', 'morb@id.slug', user_pwd);
--     end
-- $$ language plpgsql;
-- select * from users;
-- select check_password('morb@id.slug', 'secret');
-- select check_password('morb@id.slug', 'not-the-secret');
insert into users  (name, unique_name, password) values ('mark', 'regular@id.slug', 'encrypt_me_please');
select check_password('regular@id.slug', 'encrypt_me_please');

--- crypt check ---
select crypt('secret', gen_salt('bf', 8));
select crypt('secret', '<PUT SALT HERE>');


--- CLEAN UP ---
--- ======== ---
drop table users;
truncate table users;
drop function check_password(supplied_email varchar, supplied_pwd varchar);
